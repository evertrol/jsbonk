# Background

Bonken is a card game played with four people, and consists of several sub-games. Each subgame is played with a full deck of cards, and the challenge to get as many tricks as possible (for so-called plus games), or avoid certain tricks or cards (for minus games). Each trick or special card has a score (positive for plus games, negative for minus games) that results in a total score per player per subgame. At the end of twelve rounds, all scores are added together to determine the winner. There are a total of thirteen games, and no game can be played twice in a session, which means that occasionally, a player will be forced to choose a game they prefer not to play (and may thus easily lose).

In addition to the normal points-per-trick-or-card, players can bid against each other if they feel they can defeat that player. Bids can also be countered, that is, player A can bid against player B, and player B can bid against player A. When there are bids between players, the difference in points is added to each player's score won in that round: the difference is calculated from the perspective of the player for whom the score is calculated, and is thus positive for one player, while negative for the other player (or it may be a net result of zero). When both players have bid against each other, the difference is added twice. More details are in the game rules, including some examples.

The slightly complicated scoring process has resulted in mistakes keeping track of the points manually. Over the years, programs have been written to keep track of the score without making mistakes, as well preventing some other mistakes during gameplay (for example, who shuffles, who deals, who chooses a game and who leads during a subgame). This repository contains a web-app written in HTML, (S)CSS and JavaScript using the Vue.js framework.

# Credits

Credits go to Martin "Smartie" Heemskerk, for writing down the game rules, explaining the rules, and dragging other people into the game.
