"use strict;"

function readSavedGames() {
}

function selectId(id) {
	return document.getElementById(id);
}

function queryAll(selector, parent) {
	parent = parent === undefined ? document : parent;
	return parent.querySelectorAll(selector);
}

function openPage(pageName) {
	queryAll('section').forEach(item => item.style.display = 'none');
	selectId(pageName).style.display = "block";
	queryAll('.tablink').forEach(item => item.style.backgroundColor = '#2d3e4e');
	selectId(pageName + 'Tab').style.backgroundColor = '#8cbdb9';
}

const GameType = Object.freeze({'minus': '-', 'plus': '+'});

var games = [{
	name: "Points of hearts",
	short: "heartpoints",
	value: -10,
	ntricks: 13,
	type: GameType.minus,
	   }, {
	     name: "Kings & Jacks",
		 short: "kingjacks",
		 value: -25,
		 ntricks: 8,
	type: GameType.minus,
	   }, {
	     name: "King of Hearts",
		 short: "kinghearts",
		 value: -100,
		 ntricks: 1,
	type: GameType.minus,
	   }, {
	     name: "Queens",
		 short: "queens",
		 value: -45,
		 ntricks: 4,
	type: GameType.minus,
	   }, {
	     name: "Domino",
		 short: "domino",
		 value: -100,
		 ntricks: 1,
	type: GameType.minus,
	   }, {
	     name: "Duck",
		 short: "duck",
		 value: -10,
		 ntricks: 13,
	type: GameType.minus,
	   }, {
	     name: "7th & 13th trick",
		 short: "7_13",
		 value: -50,
		 ntricks: 2,
	type: GameType.minus,
	   }, {
	     name: "Last trick",
		 short: "last",
		 value: -100,
		 ntricks: 1,
	type: GameType.minus,
	   }, {
	     name: "Spades trump",
		 short: "spades",
		 value: 20,
		 ntricks: 13,
	type: GameType.plus,
	   }, {
	     name: "Hearts trump",
		 short: "hearts",
		 value: 20,
		 ntricks: 13,
	type: GameType.plus,
	   }, {
	     name: "Diamonds trump",
		 short: "diamonds",
		 value: 20,
		 ntricks: 13,
	type: GameType.plus,
	   }, {
	     name: "Clubs trump",
		 short: "clubs",
		 value: 20,
		 ntricks: 13,
	type: GameType.plus,
	   }, {
	     name: "No trump",
		 short: "trumpless",
		 value: 20,
		 ntricks: 13,
	type: GameType.plus,
	   },
	 ];

function initScores() {
	let scores = {};
	for (let game of games) {
		scores[game.short] = [null, null, null, null];
	}
	return scores;
}

function initBids() {
  return [[null, false, false, false],
    [false, null, false, false],
    [false, false, null, false],
    [false, false, false, null]];
}


function reset() {
  /* Reset all buttons, inputs and checkboxes to their starting value */
	queryAll('input.bid').forEach(item => item.disabled = true);
	queryAll('.ntricks').forEach(item => item.value = 0);

	selectId("calcScores").style.display = "none";
	selectId("nextRound").style.display = "none";
	selectId('select').style.display = 'block';
	selectId("selectGame").disabled = false;
	selectId("selectGame").selectedIndex = 0;

	/* Clear results of the previous game */
	queryAll('.chooser').forEach(el => el.style.display = 'none');
	queryAll('input.bid').forEach(el => el.checked = false);
	queryAll('td.numberOfTricks').forEach(el => el.style.display = 'none');
	queryAll('td.roundScores').forEach(el => el.style.display = 'none');
	queryAll('td.roundScores').forEach(el => el.innerHTML = '');
	for (let el of document.getElementsByClassName("numberOfTricks")) {
		el.style.display = "none";
	}

}


var app = new Vue({
	el: '#app',
	data: {
		players: [
			{
				name: "North",
				plus: false,
				nplus: 0,
				nminus: 0,
			},
			{
				name: "East",
				plus: false,
				nplus: 0,
				nminus: 0,
			},
			{
				name: "South",
				plus: false,
				nplus: 0,
				nminus: 0,
			},
			{
				name: "West",
				plus: false,
				nplus: 0,
				nminus: 0,
			}
		],
		currentPlayer: {},
		skips: [],
		scores: initScores(),
		bids: initBids(),
		currentGame: {},
		score: {
			game1: {
				player1: null,
				player2: null,
				player3: null,
				player4: null,
			},
			game2: {
				player1: 123,
				player2: null,
				player3: null,
				player4: null,
			},
		},
		icurrent: 0,
		games: games,
		gamesPlayed: new Map(),
		nGamesPlayed: 0,
		saveGames: [],
		date: undefined,
	},

	computed: {
		reversedSaveGames: function() {
			return this.saveGames.reverse();
		},
		minusGames: function() {
			let games = [];
			for (let game of this.games) {
				if (game.value > 0) {
					break;
				}
				games.push(game);
			}
			return games;
		},
		plusGames: function() {
			let games = [];
			for (let game of this.games) {
				if (game.value < 0) {
					continue;
				}
				games.push(game);
			}
			return games;
		},

	  totals: function() {
		  let players = [0, 0, 0, 0];
		  for (let score of Object.values(this.scores)) {
			  for ([index, points] of score.entries()) {
				  players[index] += points;
			  }
		  }
		  return players;
	  },
  },

	mounted: function() {
		let games = [];
		let line = document.cookie.split('; ').find(row => row.startsWith('saveGames='));
		if (line) {
			value = line.split('=')[1];
			if (value) {
				games = JSON.parse(value);
			}
		}
		this.saveGames = games;
		if (this.saveGames.length) {
			selectId('showSaveGames').style.display = 'block';
		}
	},

  methods: {
	  activePlayer: function(i) {
		  return this.currentPlayer == this.players[i] ? 'activePlayer' : '';
	  },
      selectStartingPlayer: function(event) {
		  this.icurrent = selectId("startingPlayer").selectedIndex - 1;
		  this.currentPlayer = this.players[this.icurrent];
		  selectId("start").disabled = false;
      },

	  selectSavedGame: function(event) {
		  selectId('loadGame').disabled = false;
	  },

	  start: function(event) {
		  this.date = new Date();
		  queryAll('.tablink').forEach((el) => el.style.visibility = 'visible');
		  selectId("startingPlayer").disabled = true;
		  selectId("start").disabled = true;
		  if (this.saveGames.length) {
			  selectId("saveGames").disabled = true;
			  selectId("loadGame").disabled = true;
		  }
		  this.saveGames.push({'date': this.date.toJSON(), 'game': this.gameToString()});
		  while (this.saveGames.length > 10) {
		  	  this.saveGames = this.saveGames.shift();
		  }
		  openPage('select');
	  },

	  gameToString: function() {
		  let gamesPlayed = {};
		  this.gamesPlayed.forEach((value, key) => gamesPlayed[key] = {'turn': value.turn, 'player': value.player});
		  let data = {
			  players: this.players,
			  scores: this.scores,
			  gamesPlayed: gamesPlayed,
			  icurrent: this.icurrent,
			  currentPlayer: this.currentPlayer.name,
		  };
		  let string = JSON.stringify(data)

		  return string;
	  },

	  stringToGame: function(string) {
		  let data = JSON.parse(string);
		  let scores = data['scores'];
		  let gamesPlayed = data['gamesPlayed'];
		  this.scores = scores;
		  this.players = data['players'];
		  this.gamesPlayed = new Map();
		  for (name of Object.keys(gamesPlayed)) {
		  	  let gamePlayed = gamesPlayed[name];
		  	  let game = games.filter(game => game.short == name)[0];
		  	  this.gamesPlayed.set(name, {'turn': gamePlayed.turn, 'player': gamePlayed.player});
		  }
		  this.icurrent = data['icurrent'];
		  this.currentGame = games[this.icurrent];
		  name = data['currentPlayer'];
		  for (player of this.players) {
			  if (player.name == name) {
				  this.currentPlayer = player;
				  break;
			  }
		  }
	  },

	  newGame: function(event) {
		  selectId("selectGame").disabled = true;
		  selectId("newGame").disabled = true;
		  queryAll('.chooser').forEach((el) => el.style.display = 'block');
		  selectId("confirmBids").style.display = "inline-block";

		  let index = selectId("selectGame").value;
		  this.currentGame = games[index];
		  this.gamesPlayed.set(this.currentGame.short, {'player': this.currentPlayer.name, 'turn': this.gamesPlayed.size+1});
		  queryAll('input.bid:not(.unavailable)').forEach((el) => el.style.visibility = 'visible');
		  for (let tr of queryAll(`tr.bids${this.icurrent}`)) {
			  for (let input of tr.querySelectorAll('input.bid')) {
				  input.style.visibility = "hidden";
			  }
		  }
		  if (this.currentGame.type == GameType.minus) {
			  this.currentPlayer.nminus += 1;
		  } else if (this.currentGame.type == GameType.plus) {
			  this.currentPlayer.plus = true;
			  this.currentPlayer.nplus += 1;
		  }

		  queryAll('input.bid').forEach((el) => el.disabled = false);
		  this.updateDiagram();
		  openPage("bidding");
      },

	  confirmBids: function() {
		  selectId('selectGame').selectedIndex = 0;
		  selectId('selectGame').disabled = true;
		  selectId('newGame').disabled = true;
		  selectId('select').style.display = 'none';

		  queryAll('input.bid').forEach(item => item.disabled = true);
		  queryAll('.numberOfTricks').forEach(item => item.style.display = 'table-cell');
		  selectId("confirmBids").style.display = "none";
		  selectId("calcScores").style.display = "inline-block";
		  openPage('results');
	  },

	  calcScores: function(event) {
		  return this.updateScores(event);
	  },

	  updateScores: function(event) {
	      let ntricks = 0;
		  let tricks = queryAll('.ntricks');
		  for (let el of tricks) {
			  ntricks += Number(el.value);
		  }
		  if (ntricks != this.currentGame.ntricks) {
			  alert("Number of tricks doesn't add up to " + this.currentGame.ntricks);
			  return ;
		  }
		  let score = [];
		  const value = this.currentGame.value;
		  for (let i = 0; i < 4; i++) {
			  const n = Number(tricks[i].value);
			  let p = n;
			  pos = value > 0;
			  let s = `(${n}`;
			  for (let j = 0; j < 4; j++) {
				  let m = 0 + this.bids[i][j] + this.bids[j][i];
				  if (m > 0) {
					  let diff = (n - Number(tricks[j].value));
					  if (diff != 0) {
						  let t = (m == 2) ? `2*${Math.abs(diff)}` : `${Math.abs(diff)}`;
						  s += (diff > 0) ? ' + ' : ' - ';
						  s += t;
					  }
					  p += m * diff;
				  }
			  }
			  let total = p * value;
			  s += ` = ${p}) * ${value} = ${total}`;
			  score.push(total);
			  el = selectId(`player${i}_score_td`);
			  el.innerHTML = s;
			  el.style.display = 'table-cell';
		  }
		  let sum = score.reduce((a, b) => a + b, 0);

		  this.scores[this.currentGame.short] = score;

		  if (this.gamesPlayed.size == 12) {
			  this.showWinner();
			  return ;
		  }
		  selectId("nextRound").style.display = "inline-block";
		  openPage('score');
	  },

	  nextRound: function() {
		  this.saveGame();
		  this.skips = [];
		  // Unfortunately, ES doesn't allow .filter etc on iterators; convert to an array first
		  let nmin = 0;
		  let names = Array.from(this.gamesPlayed.keys());
		  for (game of games) {
			  for (name of names) {
				  if (game.short == name && game.value < 0) {
					  nmin += 1;
					  break;
				  }
			  }
		  }
		  while (true) {
			  this.icurrent = (this.icurrent + 1) % 4;
			  this.currentPlayer = this.players[this.icurrent];
			  if (this.currentPlayer.plus && nmin == 8) {
				  this.skips.push(this.currentPlayer);
			  } else {
				  break;
			  }
		  }

		  this.bids = initBids();
		  reset();
		  openPage("select");
	  },

	  saveGame: function() {
		  this.saveGames[this.saveGames.length - 1]['game'] = this.gameToString();
		  document.cookie = 'saveGames=' + JSON.stringify(this.saveGames);
	  },

	  showWinner: function() {

		  let max = this.totals[0];
		  i = 0;
		  for ([i, value] of this.totals.entries()) {
			  if (value > max) {
				  max = value;
				  i = i;
			  }
		  }
		  let winners = [];
		  for ([i, total] of this.totals.entries()) {
			  if (max == total) {
				  winners.push(this.players[i]);
			  }
		  }
		  selectId('nextRound').style.display = 'none';
		  el = selectId('winner');
		  if (winners.length == 1) {
			  el.innerHTML = `${winners[0].name} wins!`;
		  } else if (winners.length == 2) {
			  el.innerHTML = `${winners[0].name} and ${winners[1].name} win!`;
		  } else if (winners.length == 3) {
			  el.innerHTML = `${winners[0].name}, ${winners[1].name} and ${winners[2].name} win!`;
		  } else {
			  el.innerHTML = `No-one wins!`;
		  }
		  el.style.display = 'block';

		  openPage("score");
	  },

	  updateCheckBoxes: function(event) {
		  /* Since Vue.js binds the checkmarks with this.bids, it will
		   * automatically toggle the respective value in this.bids,
		   * but only *after* this function has run. That is, after
		   * the @click method has run, the various v-bind attributes
		   * will be updated. So we cheat, and temporarily set the
		   * value in this.bids to its final value, before reverting
		   * it at the end of the function, so Vue.js can update
		   * (toggle) it properly at the very end of things. */
		  let data = event.target.dataset;
		  let ii = Number(data.i);
		  let jj = Number(data.j);
		  this.bids[ii][jj] = !this.bids[ii][jj];

		  let i = this.icurrent;
		  for (const [j, row] of this.bids.entries()) {
			  if (i != j) {
				  visibility = row[i] ? 'visible' : 'hidden';
				  selectId(`bid${i}vs${j}`).style.visibility = visibility;
				  this.bids[i][j] = (visibility == 'hidden') ? false : this.bids[i][j];
			  }
		  }
		  this.updateDiagram();
		  // Reset the value
		  this.bids[ii][jj] = !this.bids[ii][jj];
	  },

	  gamePlayed: function(game) {
		  // Require some awkward wrapping for javascript or Vue.js not to cause an error
		  let item = this.gamesPlayed.get(game.short);
		  if (item === undefined) {
			  return '';
		  }
		  return `${item.turn}; ${item.player}`;
	  },

	  resetGame: function() {
		  if (!confirm("Reset the current game?")) {
			  return ;
		  }
		  this.gamesPlayed.delete(this.currentGame.short);
		  if (this.currentGame.type == GameType.minus) {
			  this.currentPlayer.nminus -= 1;
		  } else if (this.currentGame.type == GameType.plus) {
			  this.currentPlayer.plus = false;
			  this.currentPlayer.nplus -= 1;
		  }

		  this.bids = initBids();
		  reset();
		  openPage("select");
	  },

	  loadGame: function() {
		  let i = selectId("saveGames").selectedIndex - 1;
		  this.stringToGame(this.saveGames[i].game);
		  queryAll('.tablink').forEach((el) => el.style.visibility = 'visible');
		  selectId("startingPlayer").disabled = true;
		  selectId("start").disabled = true;
		  selectId("saveGames").disabled = true;
		  selectId("loadGame").disabled = true;
		  openPage('select');
	  },

	  drawDiagram: function(n) {
		  let ns = "http://www.w3.org/2000/svg"
		  let svg = document.createElementNS(ns, 'svg')
		  svg.setAttribute('width', 300);
		  svg.setAttribute('height', 300);
		  svg.setAttribute('viewBox', '0 0 400 400');
		  let defs = document.createElementNS(ns, 'defs');
		  let marker = document.createElementNS(ns, 'marker');
		  marker.setAttribute('id', `triangle${n}`);
		  marker.setAttribute('viewBox', '0 0 10 10');
		  marker.setAttribute('refX', 10);
		  marker.setAttribute('refY', 5);
		  marker.setAttribute('markerUnits', 'strokeWidth');
		  marker.setAttribute('markerWidth', 10);
		  marker.setAttribute('markerHeight', 10);
		  marker.setAttribute('orient', 'auto');
		  let path = document.createElementNS(ns, 'path');
		  path.setAttribute('d', 'M 0 10 L 10 5 L 0 0 z');
		  path.setAttribute('fill', '#000');
		  marker.appendChild(path);
		  defs.appendChild(marker);
		  svg.appendChild(defs);
		  let x = [175, 80, 270, 175];
		  let y = [80, 175, 175, 270];
		  for (let i = 0; i < 4; i++) {
			  let rect = document.createElementNS(ns, 'rect');
			  rect.setAttribute('x', x[i]);
			  rect.setAttribute('y', y[i]);
			  rect.setAttribute('width', 50);
			  rect.setAttribute('height', 50);
			  rect.setAttribute('rx', 5);
			  rect.setAttribute('style', 'stroke: black; fill: none');
			  svg.append(rect);
		  }
		  x = [200, 330, 200, 70];
		  y = [70, 200, 330, 200];
		  let anchor = ['middle', 'start', 'middle', 'end',];
		  let baseline = ['auto', 'middle', 'hanging', 'middle'];
		  for (let i = 0; i < 4; i++) {
			  let text = document.createElementNS(ns, 'text');
			  text.setAttribute('x', x[i]);
			  text.setAttribute('y', y[i]);
			  text.setAttribute('text-anchor', anchor[i]);
			  text.setAttribute('dominant-baseline', baseline[i]);
			  text.textContent = this.players[i].name;
			  svg.append(text);
		  }
		  let x1 = [130, 175, 270, 130, 175, 80, 175, 225, 320, 225, 270, 225];
		  let y1 = [175, 80, 175, 225, 270, 225, 130, 270, 175, 130, 225, 320];
		  let x2 = [175, 80, 130, 270, 130, 175, 175, 225, 225, 270, 225, 320];
		  let y2 = [130, 175, 175, 225, 225, 320, 270, 130, 80, 175, 270, 225];
		  let index1 = [3, 0, 1, 3, 2, 3, 0, 2, 1, 0, 1, 2];
		  let index2 = [0, 3, 3, 1, 3, 2, 2, 0, 0, 1, 2, 1];
		  for (let i = 0; i < 12; i++) {
			  let line = document.createElementNS(ns, 'line');
			  line.setAttribute('x1', x1[i]);
			  line.setAttribute('y1', y1[i]);
			  line.setAttribute('x2', x2[i]);
			  line.setAttribute('y2', y2[i]);
			  line.setAttribute('stroke', 'black');
			  if (this.bids[index1[i]][index2[i]]) {
				  line.setAttribute('stroke-width', 2);
				  line.setAttribute('marker-end', `url(#triangle${n})`);
			  } else {
			  	  line.setAttribute('stroke-dasharray', '5,5');
			  }
			  svg.append(line);
		  }
		  return svg;
	  },

	  updateDiagram: function() {
		  ['Bidding', 'Results'].forEach((section, i) => {
			  let div = selectId('diagram' + section);
			  let svg = this.drawDiagram(i);
			  if (div.children.length == 0) {
				  div.append(svg);
			  } else {
				  div.children[0].replaceWith(svg);
			  }
		  })
	  },
  },
})


/* Anything below this line (inclusive) is removed in the published version */

function start() {
	var el = selectId("startingPlayer");
	el.selectedIndex = 1;
	el.dispatchEvent(new Event('change'));
	selectId('start').click();

	el = selectId('selectGame');
	el.selectedIndex = 2;
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId("bid1vs0").click();
	selectId("bid1vs2").click();
	selectId("bid1vs3").click();
	selectId("bid3vs2").click();
	selectId("bid0vs1").click();
	selectId('confirmBids').click();
	selectId('player0Tricks').value = 3;
	selectId('player1Tricks').value = 4;
	selectId('player2Tricks').value = 5;
	selectId('player3Tricks').value = 1;
////	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 11;  // spades
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId("bid2vs0").click();
	selectId("bid2vs3").click();
	selectId("bid3vs0").click();
	selectId("bid3vs1").click();
	selectId("bid3vs2").click();
	selectId("bid0vs1").click();
	selectId("bid1vs0").click();
	selectId('confirmBids').click();
	selectId('player0Tricks').value = 2;
	selectId('player1Tricks').value = 4;
	selectId('player2Tricks').value = 2;
	selectId('player3Tricks').value = 5;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 3;  // kings & jacks
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 2;
	selectId('player1Tricks').value = 2;
	selectId('player2Tricks').value = 2;
	selectId('player3Tricks').value = 2;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 12;  // hearts
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 2;
	selectId('player1Tricks').value = 4;
	selectId('player2Tricks').value = 2;
	selectId('player3Tricks').value = 5;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 13;  // diamonds
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId("bid2vs0").click();
	selectId("bid2vs3").click();
	selectId("bid3vs0").click();
	selectId("bid3vs1").click();
	selectId("bid3vs2").click();
	selectId("bid0vs1").click();
	selectId("bid1vs0").click();
	selectId('confirmBids').click();
	selectId('player0Tricks').value = 2;
	selectId('player1Tricks').value = 4;
	selectId('player2Tricks').value = 2;
	selectId('player3Tricks').value = 5;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 4;  // king of hearts
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 0;
	selectId('player1Tricks').value = 0;
	selectId('player2Tricks').value = 0;
	selectId('player3Tricks').value = 1;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 5;  // queens
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 1;
	selectId('player1Tricks').value = 1;
	selectId('player2Tricks').value = 1;
	selectId('player3Tricks').value = 1;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 6;  // domino
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 1;
	selectId('player1Tricks').value = 0;
	selectId('player2Tricks').value = 0;
	selectId('player3Tricks').value = 0;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 7;  // duck
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 2;
	selectId('player1Tricks').value = 4;
	selectId('player2Tricks').value = 2;
	selectId('player3Tricks').value = 5;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 8;  // 7th & 13th trick
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();

	selectId('confirmBids').click();
	selectId('player0Tricks').value = 0;
	selectId('player1Tricks').value = 1;
	selectId('player2Tricks').value = 1;
	selectId('player3Tricks').value = 0;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 9;  // last trick
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();
/*
	selectId('confirmBids').click();
	selectId('player0Tricks').value = 0;
	selectId('player1Tricks').value = 0;
	selectId('player2Tricks').value = 1;
	selectId('player3Tricks').value = 0;
//	selectId('updateScores').click();
	selectId('calcScores').click();

	selectId('nextRound').click();

	el = selectId('selectGame');
	el.selectedIndex = 15;  // last trick
	el.dispatchEvent(new Event('change'));
	el = selectId('newGame');
	el.click();
*/

}

//start();
