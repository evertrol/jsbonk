all: css

publish: css minimaljs html

html:
	sed 's#<script src="vue-2.6.12.js"></script>##' bonken.html > index.html
	sed -i 's#<!-- script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script -->#<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>#' index.html
	sed -i 's#<script src="bonken.js">#<script src="bonken.min.js">#' index.html

css: bonken.scss
	sassc bonken.scss bonken.css

minimaljs: bonken.js
	sed -n '/Anything below this line (inclusive) is removed in the published version/q;p' bonken.js > bonken.temp.js
	sed -i '/console.log/d' bonken.temp.js
	uglifyjs.terser bonken.temp.js > bonken.min.js
	rm bonken.temp.js
